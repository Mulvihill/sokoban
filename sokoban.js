const  nm = [  
    "  WWWWW ",  
    "WWW   W ",  
    "WOSB  W ",  
    "WWW BOW ",  
    "WOWWB W ",  
    "W W O WW",  
    "WB XBBOW",  
    "W   O  W",  
    "WWWWWWWW"  
  ];


function splitMap(badMap) {
    let newMap = [];
    for (i=0;i<badMap.length - 1;i++) {
        newMap.push(badMap[i].split(''));
    }
    return newMap;
}

cellType= function (mapStr) {
    if (mapStr== ' ') {
        return 'empty';
    }
    else if (mapStr=='W') {
        return 'wall';
    }
    else if (mapStr=='S') {
        return 'player';
    }
    else if (mapStr=='O') {
        return 'emptyStore';
    }
    else if (mapStr=='X') {
        return 'fullStore';
    }
    else if (mapStr=='B') {
        return 'empty box';
    }
    else if (mapStr=='SO') {
        return 'emptyStore player';
    }

    
    // case (mapStr == " "): 
    //     return 'empty';
    
    // case (mapStr == "W"): 
    //     return 'wall';
    
    // case (mapStr == "S"): 
    //     return 'player';
    // case (mapStr == "F"):
    //     return 'finish'
    // case (mapStr=='O'):
    //     return 'emptyStore';
    // case (mapStr=='X'):
    //     return 'fullStore';
    // case (mapStr=='B'):
    //     return 'empty box';
    // case (mapStr=='SO'):
    //     return "emptyStore player";        
    
};
sokCheckWin = function (m) {
    var boxCount = 0;
    for (r=0; r < m.length; r++ ) {
        for (c=0; c < m[r].length; c++) {
            if (m[r][c]=='O' || m[r][c]=='SO')
            {boxCount += 1;}
            if (r == m.length - 1 && c == m[r].length -1){
            if (boxCount == 0) {alert('You Win!')}
            console.log(r + ' ' + c)
            }
        }
    }
}

map = splitMap(nm);

function Map (mapArray) {
    this.mapArray = mapArray;
}

Map.prototype = {
    constructor: Map(this.mapArray),
    generateMap: function (arr) {
        gGameBoard = document.getElementById('gameMap');
        for (k = gGameBoard.childElementCount; k != 0; k--) {gGameBoard.removeChild(gGameBoard.lastElementChild)};
        for (row in arr) {
        const getMapBody = document.getElementById('gameMap');
        const initRow = document.createElement('div');
        initRow.id = 'row';
        initRow.dataset.rowCoordinate = row;
        getMapBody.appendChild(initRow);
        for (cell in arr[row]) {
            // const newCell = document.createElement('div');
            // newCell.dataset.cellCoordinate = row + "-" + cell;
            newCell = document.createElement('div');
            newCell.id = 'block';
            newCell.classList += cellType(arr[row][cell]);
            newCell.dataset.coordinates = row + '-' + cell;
            initRow.appendChild(newCell);
        }
        }
    },

    

    move: function (coord, key) {//document.querySelector('#block.player)
    coordinate = coord.split('-');
        moving = this.mapArray[coordinate[0]][coordinate[1]];
        if (moving == 'S') {
            if (key.code == 'ArrowUp') {
                    destination = this.mapArray[coordinate[0] - 1][coordinate[1]];
                    if (destination != 'W') {
                        
                        if (destination == 'B'|| destination == 'X'){
                                playerMove(coordinate[0]-1 + '-' +coordinate[1], destination, key);
                            }
                            else if (destination == ' '){
                                this.mapArray[coord[0]][coord[1]] = ' '
                                this.mapArray[coord[0]-1][coord[1]] = 'S';
                            }
                            else if (destination == 'O') {
                                this.mapArray[coord[0]][coord[1]] = ' '
                                this.mapArray[coord[0]-1][coord[1]] = 'SO';
                            }
                            else {alert('Invalid move!')}
                        }
                    }
                    
                else if(key.code == "ArrowDown"){
                        destination = this.mapArray[coordinate[0] + 1][coordinate[1]];
                        if (destination != 'W') {
                            
                            if(destination == 'B'|| destination == 'X'){
                                    this.move(coordinate[0]+1 + '-' +coordinate[1], destination, key);
                                }
                            else if (destination == ' '){
                                        this.mapArray[coord[0]][coord[1]] = ' '
                                        this.mapArray[coord[0]+1][coord[1]] = 'S';
                                    }
                                    else if (destination == 'O') {
                                        this.mapArray[coord[0]][coord[1]] = ' '
                                        this.mapArray[coord[0]+1][coord[1]] = 'SO';
                                    }
                                    else {alert('Invalid move!')}
                        }
                else if(key.code == "ArrowRight"){
                    destination = this.mapArray[coordinate[0]][coordinate[1]+1];
                
                    if (destination != 'W') {
                            if (destination == 'B'|| destination == 'X'){
                                this.move(coordinate[0] + '-' +coordinate[1] +1, destination, key);
                            }
                            else if(destination == ' '){
                                this.mapArray[coord[0]][coord[1]] = ' '
                                this.mapArray[coord[0]][coord[1]+1] = 'S';
                            }
                            else if (destination == "O"){
                                this.mapArray[coord[0]][coord[1]] = ' '
                                this.mapArray[coord[0]][coord[1]+1] = 'SO';
                            }
                            else {alert('Invalid Move!')};
                    }
                }
                else if  (key.code == "ArrowLeft"){
                        destination = this.mapArray[coordinate[0]][coordinate[1]-1];
                        if (destination != 'W') {
                            if(destination == 'B'|| destination == 'X'){
                                    this.move(coordinate[0] + '-' +coordinate[1] -1, destination, key);
                            }
                            else if (destination == ' '){
                                    this.mapArray[coord[0]][coord[1]] = ' '
                                    this.mapArray[coord[0]][coord[1]-1] = 'S';
                            }
                            else if (destination == 'O') {
                                this.mapArray[coord[0]][coord[1]-1]
                            }
                            else {alert('Invalid move!')}
                        }
                }
            }
        }
        else if (moving == 'B'){
            if (key.code == 'ArrowUp'){ 
                destination = this.mapArray[coordinate[0] - 1][coordinate[1]];
            if (destination != 'W') {
                    
                    if (destination == 'B'|| destination == 'X'){
                            this.move(coordinate[0]-1 + '-' +coordinate[1], destination, key);
                        }
                        else if (destination == ' '){
                            this.mapArray[coord[0]][coord[1]] = ' '
                            this.mapArray[coord[0]-1][coord[1]] = 'B';
                        }
                        else if (destination == 'O') {
                            this.mapArray[coord[0]][coord[1]] = ' '
                            this.mapArray[coord[0]-1][coord[1]] = 'X';
                        }
                        else {alert('Invalid move!')}
                    }
                }
                
                
            else if(key.code == "ArrowDown"){
                    destination = this.mapArray[coordinate[0] + 1][coordinate[1]];
                    if (destination != 'W') {
                        
                        if(destination == 'B'|| destination == 'X'){
                                this.move(coordinate[0]+1 + '-' +coordinate[1], destination, key);
                            }
                        else if (destination == ' '){
                                    this.mapArray[coord[0]][coord[1]] = ' '
                                    this.mapArray[coord[0]+1][coord[1]] = 'B';
                                }
                                else if (destination == 'O') {
                                    this.mapArray[coord[0]][coord[1]] = ' '
                                    this.mapArray[coord[0]+1][coord[1]] = 'X';
                                }
                                else {alert('Invalid move!')}
                            }
                    }
            else if(key.code == "ArrowRight"){
                destination = this.mapArray[coordinate[0]][coordinate[1]+1];
            
                if (destination != 'W') {
                        if (destination == 'B'|| destination == 'X'){
                            this.move(coordinate[0] + '-' +coordinate[1] +1, destination, key);
                        }
                        else if(destination == ' '){
                            this.mapArray[coord[0]][coord[1]] = ' '
                            this.mapArray[coord[0]][coord[1]+1] = 'B';
                        }
                        else if (destination == "O"){
                            this.mapArray[coord[0]][coord[1]] = ' '
                            this.mapArray[coord[0]][coord[1]+1] = 'X';
                        }
                        else {alert('Invalid Move!')};
                }
            }
            else if  (key.code == "ArrowLeft"){
                    destination = this.mapArray[coordinate[0]][coordinate[1]-1];
                    if (destination != 'W') {
                        if(destination == 'B'|| destination == 'X'){
                                this.move(coordinate[0] + '-' +coordinate[1] -1, destination, key);
                        }
                        else if (destination == ' '){
                                this.mapArray[coord[0]][coord[1]] = ' '
                                this.mapArray[coord[0]][coord[1]-1] = 'B';
                        }
                        else if (destination == 'O') {
                            this.mapArray[coord[0]][coord[1]] = ' '
                            this.mapArray[coord[0]][coord[1]-1] = 'X'
                        }
                        else {alert('Invalid move!')}
                    }
            }
        }

        
        else if (moving == 'X'){
            if (key.code == 'ArrowUp'){ 
                destination = this.mapArray[coordinate[0] - 1][coordinate[1]];
            if (destination != 'W') {
                    if (destination == 'B'|| destination == 'X'){
                            this.move(coordinate[0]-1 + '-' +coordinate[1], destination, key);
                        }
                        else if (destination == ' '){
                            this.mapArray[coord[0]][coord[1]] = 'O'
                            this.mapArray[coord[0]-1][coord[1]] = 'B';
                        }
                        else if (destination == 'O') {
                            this.mapArray[coord[0]][coord[1]] = 'O'
                            this.mapArray[coord[0]-1][coord[1]] = 'X';
                        }
                        else {alert('Invalid move!')}
                    }
                }
                
                
            else if(key.code == "ArrowDown"){
                    destination = this.mapArray[coordinate[0] + 1][coordinate[1]];
                    if (destination != 'W') {
                        
                        if(destination == 'B'|| destination == 'X'){
                                this.move(coordinate[0]+1 + '-' +coordinate[1], destination, key);
                            }
                        else if (destination == ' '){
                                    this.mapArray[coord[0]][coord[1]] = 'O'
                                    this.mapArray[coord[0]+1][coord[1]] = 'B';
                                }
                                else if (destination == 'O') {
                                    this.mapArray[coord[0]][coord[1]] = 'O'
                                    this.mapArray[coord[0]+1][coord[1]] = 'X';
                                }
                                else {alert('Invalid move!')}
                            }
                    }
            else if(key.code == "ArrowRight"){
                destination = this.mapArray[coordinate[0]][coordinate[1]+1];
            
                if (destination != 'W') {
                        if (destination == 'B'|| destination == 'X'){
                            this.move(coordinate[0] + '-' +coordinate[1] +1, destination, key);
                        }
                        else if(destination == ' '){
                            this.mapArray[coord[0]][coord[1]] = 'O'
                            this.mapArray[coord[0]][coord[1]+1] = 'B';
                        }
                        else if (destination == "O"){
                            this.mapArray[coord[0]][coord[1]] = 'O'
                            this.mapArray[coord[0]][coord[1]+1] = 'X';
                        }
                        else {alert('Invalid Move!')};
                }
            }
            else if  (key.code == "ArrowLeft"){
                    destination = this.mapArray[coordinate[0]][coordinate[1]-1];
                    if (destination != 'W') {
                        if(destination == 'B'|| destination == 'X'){
                                this.move(coordinate[0] + '-' +coordinate[1] -1, destination, key);
                        }
                        else if (destination == ' '){
                                this.mapArray[coord[0]][coord[1]] = 'O'
                                this.mapArray[coord[0]][coord[1]-1] = 'B';
                        }
                        else if (destination == 'O') {
                            this.mapArray[coord[0]][coord[1]] = 'O'
                            this.mapArray[coord[0]][coord[1]-1] = 'X'
                        }
                        else {alert('Invalid move!')}
                    }
            }
        }
        
        this.generateMap(this.mapArray);
    },
    createCell: function (type, coordinates) {
        newCell = document.createElement('div');
        newCell.id = coordinates;
        newCell.class = cellType(type);
        return newCell;
    },

    playerMove: function (key) {
        this.move(document.querySelector('#block.player').dataset.coordinates, key);
    }
    
}

function Cell (type, coordinates) {
    this.type = type;
    this.coordinates = coordinates;
    
}   
// Cell.prototype = {
//     constructor: Cell(this.type, this.coordinates),

//     genElement: function() {
//         newCell = document.createElement('div');
//         newCell.id = this.coordinates;
//         newCell.class = cellType(this.type);
//     },

    
// }
var qCor = '';
verifyMove = function (cord, direction) {
    if (direction == 'ArrowUp') {
        qCor = map[cord[0]-2][cord[1]]
        if (qCor == ' ' || qCor == 'O')
        {
            return true;
        } 
        else {return false;}
    }
    else if (direction == 'ArrowDown') {
        qCor = map[cord[0]+2][cord[1]];
        if (qCor == ' ' || qCor == 'O') {
            return true;
        }
        else {return false;}
    }
    else if (direction == 'ArrowRight') {
        qCor = map[cord[0]][cord[1]+2];
        console.log(qCor)
        if (qCor == ' ' || qCor == 'O') {
            return true;
        }
        else {return false;}
    }
    else if(direction == 'ArrowLeft') {
        qCor = map[cord[0]][cord[1]-2];
        if (qCor == ' ' || qCor == 'O') {
            return true;
        }
        else {return false;}
    }
}

playerMove = function (key) {//document.querySelector('#block.player)
console.log(map[2][2])
    coord = findPlayerCoordinates();
    coordinate = coord.split('-');
    coordinate[0] = Number(coordinate[0]);
    coordinate[1] = Number(coordinate[1]);
    var moving = map[coordinate[0]][coordinate[1]];
    var destination;
    if (moving == 'S') {
        console.log('S')
        
        if (key.code == 'ArrowUp') {
                destination = map[coordinate[0] - 1][coordinate[1]];
                if (destination != 'W') {
                    
                    if (destination == 'B'|| destination == 'X'){
                            if (verifyMove(coordinate, key.code))
                            {regularMove((coordinate[0]-1) + '-' + coordinate[1], key);}
                            else {alert('Invalid Move!')}
                        }
                        else if (destination == ' '){
                            map[coordinate[0]][coordinate[1]] = ' '
                            map[coordinate[0]-1][coordinate[1]] = 'S';
                        }
                        else if (destination == 'O') {
                            map[coordinate[0]][coordinate[1]] = ' '
                            map[coordinate[0]-1][coordinate[1]] = 'SO';
                        }
                        else {alert('Invalid move!')}
                    }
                }
                
            else if(key.code == "ArrowDown"){
                    destination = map[coordinate[0] + 1][coordinate[1]];
                    if (destination != 'W') {
                        
                        if(destination == 'B'|| destination == 'X'){
                            if (verifyMove(coordinate, key.code))
                                {regularMove((coordinate[0]+1) + '-' +coordinate[1], key);}
                                else {alert('Invalid Move')};
                            }
                        else if (destination == ' '){
                                    map[coordinate[0]][coordinate[1]] = ' '
                                    map[coordinate[0]+1][coordinate[1]] = 'S';
                                }
                                else if (destination == 'O') {
                                    map[coordinate[0]][coordinate[1]] = ' '
                                    map[coordinate[0]+1][coordinate[1]] = 'SO';
                                }
                                else {alert('Invalid move!')}
                    }
                }
            else if(key.code == "ArrowRight"){
                destination = map[coordinate[0]][coordinate[1]+1];
                if (destination != 'W') {
                        if (destination == 'B'|| destination == 'X'){
                            if (verifyMove(coordinate, key.code))
                            {regularMove(coordinate[0] + '-' +(coordinate[1] +1), key);}
                            else {alert('Invalid Move')}
                        }
                        else if(destination == ' '){
                            map[coordinate[0]][coordinate[1]] = ' '
                            map[coordinate[0]][coordinate[1]+1] = 'S';
                        }
                        else if (destination == "O"){
                            map[coordinate[0]][coordinate[1]] = ' '
                            map[coordinate[0]][coordinate[1]+1] = 'SO';
                        }}
                        else {alert('Invalid Move!')};
                    
                
            }
            else if  (key.code == "ArrowLeft"){
                    destination = map[coordinate[0]][coordinate[1]-1];
                    console.log(destination)
                    if (destination != 'W') {
                        if(destination == 'B'|| destination == 'X'){
                            if (verifyMove(coordinate, key.code))
                                {regularMove(coordinate[0] + '-' +(coordinate[1] -1), key);}
                                else {alert('Invalid Move')}
                        }
                        else if (destination == ' '){
                                map[coordinate[0]][coordinate[1]] = ' '
                                map[coordinate[0]][coordinate[1]-1] = 'S';
                        }
                        else if (destination == 'O') {
                            map[coordinate[0]][coordinate[1]] = ' ';
                            map[coordinate[0]][coordinate[1]-1] = 'SO';
                        }
                        else {alert('Invalid move!')}
                    }
            }
        }
            else if (moving == 'B'){
        if (key.code == 'ArrowUp'){ 
            destination = map[coordinate[0] - 1][coordinate[1]];
        if (destination != 'W') {
                
                if (destination == 'B'|| destination == 'X'){
                    if (verifyMove(coordinate, key.code))
                        {regularMove((coordinate[0]-1) + '-' + coordinate[1], key);}
                        else {alert('Invalid Move')}
                    }
                    else if (destination == ' '){
                        map[coordinate[0]][coordinate[1]] = ' '
                        map[coordinate[0]-1][coordinate[1]] = 'B';
                    }
                    else if (destination == 'O') {
                        map[coordinate[0]][coordinate[1]] = ' '
                        map[coordinate[0]-1][coordinate[1]] = 'X';
                    }
                    else {alert('Invalid move!')}
                }
            }
            
            
        else if(key.code == "ArrowDown"){
                destination = map[coordinate[0] + 1][coordinate[1]];
                if (destination != 'W') {
                    
                    if(destination == 'B'|| destination == 'X'){
                        if (verifyMove(coordinate, key.code))
                            {regularMove((coordinate[0]+1) + '-' +coordinate[1], key);}
                            else {alert('Invalid Move')}
                        }
                    else if (destination == ' '){
                                map[coordinate[0]][coordinate[1]] = ' '
                                map[coordinate[0]+1][coordinate[1]] = 'B';
                            }
                            else if (destination == 'O') {
                                map[coordinate[0]][coordinate[1]] = ' '
                                map[coordinate[0]+1][coordinate[1]] = 'X';
                            }
                            else {alert('Invalid move!')}
                        }
                }
        else if(key.code == "ArrowRight"){
            destination = map[coordinate[0]][coordinate[1]+1];
        
            if (destination != 'W') {
                    if (destination == 'B'|| destination == 'X'){
                        if (verifyMove(coordinate, key.code))
                        {regularMove(coordinate[0] + '-' +(coordinate[1] +1), key);}
                        else {alert('Invalid Move')}
                    }
                    else if(destination == ' '){
                        map[coordinate[0]][coordinate[1]] = ' '
                        map[coordinate[0]][coordinate[1]+1] = 'B';
                    }
                    else if (destination == "O"){
                        map[coordinate[0]][coordinate[1]] = ' '
                        map[coordinate[0]][coordinate[1]+1] = 'X';
                    }
                    else {alert('Invalid Move!')};
            }
        }
        else if  (key.code == "ArrowLeft"){
                destination = map[coordinate[0]][coordinate[1]-1];
                if (destination != 'W') {
                    if(destination == 'B'|| destination == 'X'){
                        if (verifyMove(coordinate, key.code))
                            {regularMove(coordinate[0] + '-' +(coordinate[1] -1), key);}
                            else {'invalid move'}
                    }
                    else if (destination == ' '){
                            map[coordinate[0]][coordinate[1]] = ' '
                            map[coordinate[0]][coordinate[1]-1] = 'B';
                    }
                    else if (destination == 'O') {
                        map[coordinate[0]][coordinate[1]] = ' '
                        map[coordinate[0]][coordinate[1]-1] = 'X'
                    }
                    else {alert('Invalid move!')}
                }
        }
    }

    
    else if (moving == 'SO'){
        if (key.code == 'ArrowUp'){ 
            destination = map[coordinate[0] - 1][coordinate[1]];
        if (destination != 'W') {
                if (destination == 'B'|| destination == 'X'){
                    if (verifyMove(coordinate, key.code))
                        {regularMove((coordinate[0]-1) + '-' + coordinate[1], key);}
                        else {alert('Invalid Move')}
                    }
                    else if (destination == ' '){
                        map[coordinate[0]][coordinate[1]] = 'O'
                        map[coordinate[0]-1][coordinate[1]] = 'S';
                    }
                    else if (destination == 'O') {
                        map[coordinate[0]][coordinate[1]] = 'O'
                        map[coordinate[0]-1][coordinate[1]] = 'SO';
                    }
                    else {alert('Invalid move!')}
                }
            }
            
            
        else if(key.code == "ArrowDown"){
                destination = map[coordinate[0] + 1][coordinate[1]];
                if (destination != 'W') {
                    
                    if(destination == 'B'|| destination == 'X'){
                        if (verifyMove(coordinate, key.code))
                            {regularMove((coordinate[0]+1) + '-' +coordinate[1], key);}
                            else{alert('Invalid Move')}
                        }
                    else if (destination == ' '){
                                map[coordinate[0]][coordinate[1]] = 'O'
                                map[coordinate[0]+1][coordinate[1]] = 'S';
                            }
                            else if (destination == 'O') {
                                map[coordinate[0]][coordinate[1]] = 'O'
                                map[coordinate[0]+1][coordinate[1]] = 'SO';
                            }
                            else {alert('Invalid move!')}
                        }
                }
        else if(key.code == "ArrowRight"){
            destination = map[coordinate[0]][coordinate[1]+1];
        
            if (destination != 'W') {
                    if (destination == 'B'|| destination == 'X'){
                        if (verifyMove(coordinate, key.code))
                        {regularMove(coordinate[0] + '-' +(coordinate[1] +1), key);}
                        else {alert('Invalid Move')}
                    }
                    else if(destination == ' '){
                        map[coordinate[0]][coordinate[1]] = 'O'
                        map[coordinate[0]][coordinate[1]+1] = 'S';
                    }
                    else if (destination == "O"){
                        map[coordinate[0]][coordinate[1]] = 'O'
                        map[coordinate[0]][coordinate[1]+1] = 'SO';
                    }
                    else {alert('Invalid Move!')};
            }
        }
        else if  (key.code == "ArrowLeft"){
                destination = map[coordinate[0]][coordinate[1]-1];
                if (destination != 'W') {
                    if(destination == 'B'|| destination == 'X'){
                        if (verifyMove(coordinate, key.code))
                            {regularMove(coordinate[0] + '-' +(coordinate[1] -1), key);}
                            else (alert('Invalid Move'))
                    }
                    else if (destination == ' '){
                            map[coordinate[0]][coordinate[1]] = 'O'
                            map[coordinate[0]][coordinate[1]-1] = 'S';
                    }
                    else if (destination == 'O') {
                        map[coordinate[0]][coordinate[1]] = 'O'
                        map[coordinate[0]][coordinate[1]-1] = 'SO'
                    }
                    else {alert('Invalid move!')}
                }
        }
    }
  
    generateMap(map);
}

regularMove = function (cord, key) {
   
    coordinate = cord.split('-');
    coordinate[0] = Number(coordinate[0]);
    coordinate[1] = Number(coordinate[1]);
    var moving = map[coordinate[0]][coordinate[1]];
    console.log(moving)
    if (moving == 'S') {
        if (key.code == 'ArrowUp') {
                destination = map[coordinate[0] - 1][coordinate[1]];
                if (destination != 'W') {
                   
                    if (destination == 'B'|| destination == 'X'){
                            regularMove((coordinate[0]-1) + '-' + coordinate[1], key);
                        }
                        else if (destination == ' '){
                            map[coordinate[0]][coordinate[1]] = ' '
                            map[coordinate[0]-1][coordinate[1]] = 'S';
                        }
                        else if (destination == 'O') {
                            map[coordinate[0]][coordinate[1]] = ' '
                            map[coordinate[0]-1][coordinate[1]] = 'SO';
                        }
                        
                    }
                    else {alert('Invalid move!')}
                }
                
            else if(key.code == "ArrowDown"){
                    destination = map[coordinate[0] + 1][coordinate[1]];
                    if (destination != 'W') {
                        
                        if(destination == 'B'|| destination == 'X'){
                                regularMove((coordinate[0]+1) + '-' +coordinate[1], key);
                            }
                        else if (destination == ' '){
                                    map[coordinate[0]][coordinate[1]] = ' '
                                    map[coordinate[0]+1][coordinate[1]] = 'S';
                                }
                                else if (destination == 'O') {
                                    map[coordinate[0]][coordinate[1]] = ' '
                                    map[coordinate[0]+1][coordinate[1]] = 'SO';
                                }
                                
                    }
                    else {alert('Invalid move!')}
                }
            else if(key.code == "ArrowRight"){
                destination = map[coordinate[0]][coordinate[1]+1];
            console.log(coordinate)
                if (destination != 'W') {
                        if (destination == 'B'|| destination == 'X'){
                            regularMove(coordinate[0] + '-' +(coordinate[1] +1), key);
                        }
                        else if(destination == ' '){
                            map[coordinate[0]][coordinate[1]] = ' '
                            map[coordinate[0]][coordinate[1]+1] = 'S';
                        }
                        else if (destination == "O"){
                            map[coordinate[0]][coordinate[1]] = ' '
                            map[coordinate[0]][coordinate[1]+1] = 'SO';
                        }
                        
                }
                else {alert('Invalid Move!')};
            }
            else if  (key.code == "ArrowLeft"){
                    destination = map[coordinate[0]][coordinate[1]-1];
                    if (destination != 'W') {
                        if(destination == 'B'|| destination == 'X'){
                                regularMove(coordinate[0] + '-' +(coordinate[1] -1), key);
                        }
                        else if (destination == ' '){
                                map[coordinate[0]][coordinate[1]] = ' '
                                map[coordinate[0]][coordinate[1]-1] = 'S';
                        }
                        else if (destination == 'O') {
                            map[coordinate[0]][coordinate[1]-1]
                        }
                        
                    }
                    else {alert('Invalid move!')}
            }
        
    }
    else if (moving == 'B'){
        
        if (key.code == 'ArrowUp'){ 
            destination = map[coordinate[0] - 1][coordinate[1]];
            
        if (destination != 'W') {
                
                if (destination == 'B'|| destination == 'X'){
                        regularMove((coordinate[0]-1) + '-' + coordinate[1], key);
                    }
                    else if (destination == ' '){
                        map[coordinate[0]][coordinate[1]] = ' '
                        map[coordinate[0]-1][coordinate[1]] = 'B';
                    }
                    else if (destination == 'O') {
                        map[coordinate[0]][coordinate[1]] = ' '
                        map[coordinate[0]-1][coordinate[1]] = 'X';
                    }
                    else {alert('Invalid move!')}
                }
            }
            
            
        else if(key.code == "ArrowDown"){
                destination = map[coordinate[0] + 1][coordinate[1]];
                if (destination != 'W') {
                    
                    if(destination == 'B'|| destination == 'X'){
                            regularMove((coordinate[0]+1) + '-' +coordinate[1], key);
                        }
                    else if (destination == ' '){
                                map[coordinate[0]][coordinate[1]] = ' '
                                map[coordinate[0]+1][coordinate[1]] = 'B';
                            }
                            else if (destination == 'O') {
                                map[coordinate[0]][coordinate[1]] = ' '
                                map[coordinate[0]+1][coordinate[1]] = 'X';
                            }
                            
                        }
                        else {alert('Invalid move!')}
                }
        else if(key.code == "ArrowRight"){
            destination = map[coordinate[0]][coordinate[1]+1];
            if (destination != 'W') {
                    if (destination == 'B'|| destination == 'X'){
                        regularMove(coordinate[0] + '-' +(coordinate[1] +1), key);
                    }
                    else if(destination == ' '){
                        console.log(coord)
                        map[coordinate[0]][coordinate[1]] = ' '
                        map[coordinate[0]][coordinate[1]+1] = 'B';
                    }
                    else if (destination == "O"){
                        map[coordinate[0]][coordinate[1]] = ' '
                        map[coordinate[0]][coordinate[1]+1] = 'X';
                    }
                    
            }
            else {alert('Invalid Move!')};
        }
        else if  (key.code == "ArrowLeft"){
                destination = map[coordinate[0]][coordinate[1]-1];
                if (destination != 'W') {
                    if(destination == 'B'|| destination == 'X'){
                            regularMove(coordinate[0] + '-' +(coordinate[1] -1), key);
                    }
                    else if (destination == ' '){
                            map[coordinate[0]][coordinate[1]] = ' '
                            map[coordinate[0]][coordinate[1]-1] = 'B';
                    }
                    else if (destination == 'O') {
                        map[coordinate[0]][coordinate[1]] = ' '
                        map[coordinate[0]][coordinate[1]-1] = 'X'
                    }
                    
                }
                else {alert('Invalid move!')}
        }
    }

    
    else if (moving == 'X'){
        if (key.code == 'ArrowUp'){ 
            destination = map[coordinate[0] - 1][coordinate[1]];
        if (destination != 'W') {
                if (destination == 'B'|| destination == 'X'){
                        regularMove((coordinate[0]-1) + '-' + coordinate[1], key);
                    }
                    else if (destination == ' '){
                        map[coordinate[0]][coordinate[1]] = 'O'
                        map[coordinate[0]-1][coordinate[1]] = 'B';
                    }
                    else if (destination == 'O') {
                        map[coordinate[0]][coordinate[1]] = 'O'
                        map[coordinate[0]-1][coordinate[1]] = 'X';
                    }
                    
                }
                else {alert('Invalid move!')}
            }
            
            
        else if(key.code == "ArrowDown"){
                destination = map[coordinate[0] + 1][coordinate[1]];
                if (destination != 'W') {
                    
                    if(destination == 'B'|| destination == 'X'){
                            regularMove((coordinate[0]+1) + '-' +coordinate[1], key);
                        }
                    else if (destination == ' '){
                                map[coordinate[0]][coordinate[1]] = 'O'
                                map[coordinate[0]+1][coordinate[1]] = 'B';
                            }
                            else if (destination == 'O') {
                                map[coordinate[0]][coordinate[1]] = 'O'
                                map[coordinate[0]+1][coordinate[1]] = 'X';
                            }
                            
                        }
                        else {alert('Invalid move!')}
                }
        else if(key.code == "ArrowRight"){
            destination = map[coordinate[0]][coordinate[1]+1];
        console.log(destination)
            if (destination != 'W') {
                    if (destination == 'B'|| destination == 'X'){
                        regularMove(coordinate[0] + '-' +(coordinate[1] +1), key);
                    }
                    else if(destination == ' '){
                        map[coordinate[0]][coordinate[1]] = 'O'
                        map[coordinate[0]][coordinate[1]+1] = 'B';
                    }
                    else if (destination == "O"){
                        map[coordinate[0]][coordinate[1]] = 'O'
                        map[coordinate[0]][coordinate[1]+1] = 'X';
                    }
                    
            }
            else {alert('Invalid Move!')};
        }
        else if  (key.code == "ArrowLeft"){
                destination = map[coordinate[0]][coordinate[1]-1];
                if (destination != 'W') {
                    if(destination == 'B'|| destination == 'X'){
                            regularMove(coordinate[0] + '-' +(coordinate[1] -1), key);
                    }
                    else if (destination == ' '){
                            map[coordinate[0]][coordinate[1]] = 'O'
                            map[coordinate[0]][coordinate[1]-1] = 'B';
                    }
                    else if (destination == 'O') {
                        map[coordinate[0]][coordinate[1]] = 'O'
                        map[coordinate[0]][coordinate[1]-1] = 'X'
                    }
                    
                }
                else {alert('Invalid move!')}
        }
    }
    
    generateMap(map);
}


generateMap = function (arr) {
    gGameBoard = document.getElementById('gameMap');
    for (k = gGameBoard.childElementCount; k != 0; k--) {gGameBoard.removeChild(gGameBoard.lastElementChild)};
    for (row in arr) {
    const getMapBody = document.getElementById('gameMap');
    const initRow = document.createElement('div');
    initRow.id = 'row';
    initRow.dataset.rowCoordinate = row;
    getMapBody.appendChild(initRow);
    for (cell in arr[row]) {
        // const newCell = document.createElement('div');
        // newCell.dataset.cellCoordinate = row + "-" + cell;
        newCell = document.createElement('div');
        newCell.id = 'block';
        newCell.classList += cellType(arr[row][cell]);
        newCell.dataset.coordinates = row + '-' + cell;
        initRow.appendChild(newCell);
    }
    }
    sokCheckWin(arr)
}

findPlayerCoordinates = function () {
    for (i in map) {
        for (n in map[i]) {
            if (map[i][n]=='S'||map[i][n]=='SO') {
                return i + '-' + n;
            }
        }
    }
}

generateMap(map);
document.addEventListener("keydown", playerMove);



//Sorry for the mess