// var ss = [
//     "WWWWWWWWWWWWWWWWWWWWW",
//     "W   W     W     W W W",
//     "W W W WWW WWWWW W W W",
//     "W W W   W     W W   W",
//     "W WWWWWWW W WWW W W W",
//     "W         W     W W W",
//     "W WWW WWWWW WWWWW W W",
//     "W W   W   W W     W W",
//     "W WWWWW W W W WWW W F",
//     "S     W W W W W W WWW",
//     "WWWWW W W W W W W W W",
//     "W     W W W   W W W W",
//     "W WWWWWWW WWWWW W W W",
//     "W       W       W   W",
//     "WWWWWWWWWWWWWWWWWWWWW",
// ];

const  gMap = [  
    "  WWWWW ",  
    "WWW   W ",  
    "WOSB  W ",  
    "WWW BOW ",  
    "WOWWB W ",  
    "W W O WW",  
    "WB XBBOW",  
    "W   O  W",  
    "WWWWWWWW"  
  ];
function movingObject(name, type, position) {
    this.name = name;
    this.type= type;
    this.position=position;
}
movingObject.prototype.constructor = new movingObject(name);
movingObject.prototype = {
    //constructor: movingObject,
    newPos: function(representation, mapChange){
        for (row in mapChange){
            for (cell in mapChange[row]) {
                if (mapChange[row][cell]===representation){
                    this.position= row + '-' + cell;
                    return this.position;
                }
                else {continue;}
            }
        }
    },

    

    move: function (dir, dubArr){
        let coordArr =this.position.split('-');
        coordArr[0] = Number(coordArr[0]);
        coordArr[1] = Number(coordArr[1]);
        if (this.type == 'player'){
            switch(dir){
                case (dir === 'ArrowUp'):
                    destination = dubArr[coordArr[0]-1][coordArr[1]];
                    if (destination != 'W'&& destination != null){
                        switch (destination) {
                        case (destination == 'B'):       //boxes elems should have data-object=`boxX-X`
                            var boxObjectCall = document.querySelector('#' + (coordArr[0]-1) + '-' + (coordArr[1]));
                            boxObjectCall.dataset.object.move('ArrowUp',dubArr);
                            this.move('ArrowUp', dubArr);
                        
                        case (dubArr[coordArr[0]][coordArr[1]] == 'SO' && destination == 'O'):
                            dubArr[coordArr[0]][coordArr[1]] = 'O';
                            dubArr[coordArr[0]-1][coordArr[1]] = 'SO';
                            
                        case (destination=='O'):
                            dubArr[coordArr[0]][coordArr[1]] = ' ';
                            dubArr[coordArr[0]-1][coordArr[1]] = 'SO';
                        case (dubArr[coordArr[0]][coordArr[1]] == 'SO' && destination==' '):
                            dubArr[coordArr[0]][coordArr[1]] = 'O';
                            dubArr[coordArr[0]-1][coordArr[1]] = 'S';
                        case (destination==' '):
                            dubArr[coordArr[0]][coordArr[1]] = ' ';
                            dubArr[coordArr[0]-1][coordArr[1]] = 'S';
                        }
                    
                    }
                    else {alert('Invalid Move')};
                case (dir === 'ArrowRight'):
                destination = dubArr[coordArr[0]][coordArr[1]+1];
                    if (destination != 'W'&& destination != null){
                    switch (destination) {
                    case (destination == 'B'):       //boxes elems should have data-object=`boxX-X`
                        var boxObjectCall = document.querySelector('#' + (coordArr[0]) + '-' + (coordArr[1]+1));
                        boxObjectCall.dataset.object.move('ArrowRight',dubArr);
                        this.move('ArrowRight', dubArr);
                        
                    case (dubArr[coordArr[0]][coordArr[1]] == 'SO' && destination == 'O'):
                        dubArr[coordArr[0]][coordArr[1]] = 'O';
                        dubArr[coordArr[0]][coordArr[1]+1] = 'SO';
                        
                    case (destination=='O'):
                        dubArr[coordArr[0]][coordArr[1]] = ' ';
                        dubArr[coordArr[0]][coordArr[1]+1] = 'SO';
                    case (dubArr[coordArr[0]][coordArr[1]] == 'SO' && destination==' '):
                        dubArr[coordArr[0]][coordArr[1]] = 'O';
                        dubArr[coordArr[0]][coordArr[1]+1] = 'S';
                    case (destination==' '):
                        dubArr[coordArr[0]][coordArr[1]] = ' ';
                        dubArr[coordArr[0]][coordArr[1]+1] = 'S';
                    };
                }
                else {alert('Invalid Move')};
                case (dir === 'ArrowDown'):
                    destination = dubArr[coordArr[0]+1][coordArr[1]];
                        if (destination != 'W'&& destination != null){
                        switch (destination) {
                        case (destination == 'B'):       //boxes elems should have data-object=`boxX-X`
                            var boxObjectCall = document.querySelector('#' + (coordArr[0]+1) + '-' + (coordArr[1]));
                            boxObjectCall.dataset.object.move('ArrowDown',dubArr);
                            this.move('ArrowDown', dubArr);
                            
                        case (dubArr[coordArr[0]][coordArr[1]] == 'SO' && destination == 'O'):
                            dubArr[coordArr[0]][coordArr[1]] = 'O';
                            dubArr[coordArr[0]+1][coordArr[1]] = 'SO';
                            
                        case (destination=='O'):
                            dubArr[coordArr[0]][coordArr[1]] = ' ';
                            dubArr[coordArr[0]+1][coordArr[1]] = 'SO';
                        case (dubArr[coordArr[0]][coordArr[1]] == 'SO' && destination==' '):
                            dubArr[coordArr[0]][coordArr[1]] = 'O';
                            dubArr[coordArr[0]+1][coordArr[1]] = 'S';
                        case (destination==' '):
                            dubArr[coordArr[0]][coordArr[1]] = ' ';
                            dubArr[coordArr[0]+1][coordArr[1]] = 'S';
                        };
                    }
                    else {alert('Invalid Move')};
                    case (dir === 'ArrowLeft'):
                        destination = dubArr[coordArr[0]][coordArr[1]-1];
                            if (destination != 'W'&& destination != null){
                            switch (destination) {
                            case (destination == 'B'):       //boxes elems should have data-object=`boxX-X`
                                var boxObjectCall = document.querySelector('#' + (coordArr[0]) + '-' + (coordArr[1]-1));
                                boxObjectCall.dataset.object.move('ArrowLeft',dubArr);
                                this.move('ArrowLeft', dubArr);
                                
                            case (dubArr[coordArr[0]][coordArr[1]] == 'SO' && destination == 'O'):
                                dubArr[coordArr[0]][coordArr[1]] = 'O';
                                dubArr[coordArr[0]][coordArr[1]-1] = 'SO';
                                
                            case (destination=='O'):
                                dubArr[coordArr[0]][coordArr[1]] = ' ';
                                dubArr[coordArr[0]][coordArr[1]-1] = 'SO';
                            case (dubArr[coordArr[0]][coordArr[1]] == 'SO' && destination==' '):
                                dubArr[coordArr[0]][coordArr[1]] = 'O';
                                dubArr[coordArr[0]][coordArr[1]-1] = 'S';
                            case (destination==' '):
                                dubArr[coordArr[0]][coordArr[1]] = ' ';
                                dubArr[coordArr[0]][coordArr[1]-1] = 'S';
                            };
                        }
                        else {alert('Invalid Move')};
            }
                
        }//YOU ARE HERE, reorganize the box to change into X when in storage
        else if (this.type== 'box') {
            switch(dir){
                case (dir === 'ArrowUp'):
                destination = dubArr[coordArr[0]-1][coordArr[1]];
                    if (destination != 'W'&& destination != null){
                        switch (destination) {
                        case (destination == 'B'):       //boxes elems should have data-object=`boxX-X`
                            var boxObjectCall = document.querySelector('#' + (coordArr[0]-1) + '-' + (coordArr[1]));
                            boxObjectCall.dataset.object.move('ArrowUp',dubArr);
                            this.move('ArrowUp', dubArr);
                            
                        case (destination=='O'):
                            dubArr[coordArr[0]][coordArr[1]] = ' ';
                            dubArr[coordArr[0]-1][coordArr[1]] = 'X';
                        
                        case (destination==' '):
                            dubArr[coordArr[0]][coordArr[1]] = ' ';
                            dubArr[coordArr[0]-1][coordArr[1]] = 'B';
                        }
                    
                    }
                    else {alert('Invalid Move')};
                case (dir === 'ArrowRight'):
                destination = dubArr[coordArr[0]][coordArr[1]+1];
                    if (destination != 'W'&& destination != null){
                    switch (destination) {
                    case (destination == 'B'):       //boxes elems should have data-object=`boxX-X`
                        var boxObjectCall = document.querySelector('#' + (coordArr[0]) + '-' + (coordArr[1]+1));
                        boxObjectCall.dataset.object.move('ArrowRight',dubArr);
                        this.move('ArrowRight', dubArr);
                        
                    
                        
                    case (destination=='O'):
                        dubArr[coordArr[0]][coordArr[1]] = ' ';
                        dubArr[coordArr[0]][coordArr[1]+1] = 'X';
                    
                    case (destination==' '):
                        dubArr[coordArr[0]][coordArr[1]] = ' ';
                        dubArr[coordArr[0]][coordArr[1]+1] = 'B';
                    };
                }
                else {alert('Invalid Move')};
                case (dir === 'ArrowDown'):
                    destination = dubArr[coordArr[0]+1][coordArr[1]];
                        if (destination != 'W'&& destination != null){
                        switch (destination) {
                        case (destination == 'B'):       //boxes elems should have data-object=`boxX-X`
                            var boxObjectCall = document.querySelector('#' + (coordArr[0]+1) + '-' + (coordArr[1]));
                            boxObjectCall.dataset.object.move('ArrowDown',dubArr);
                            this.move('ArrowDown', dubArr);
                            
                        
                            
                        case (destination=='O'):
                            dubArr[coordArr[0]][coordArr[1]] = ' ';
                            dubArr[coordArr[0]+1][coordArr[1]] = 'X';
                        
                        case (destination==' '):
                            dubArr[coordArr[0]][coordArr[1]] = ' ';
                            dubArr[coordArr[0]+1][coordArr[1]] = 'B';
                        };
                    }
                    else{alert('Invalid Move')};
                    case (dir === 'ArrowLeft'):
                        destination = dubArr[coordArr[0]][coordArr[1]-1];
                            if (destination != 'W'&& destination != null){
                            switch (destination) {
                            case (destination == 'B'):       //boxes elems should have data-object=`boxX-X`
                                var boxObjectCall = document.querySelector('#' + (coordArr[0]) + '-' + (coordArr[1]-1));
                                boxObjectCall.dataset.object.move('ArrowLeft',dubArr);
                                this.move('ArrowLeft', dubArr);
                                
                                
                            case (destination=='O'):
                                dubArr[coordArr[0]][coordArr[1]] = ' ';
                                dubArr[coordArr[0]][coordArr[1]-1] = 'X';
                            
                            case (destination==' '):
                                dubArr[coordArr[0]][coordArr[1]] = ' ';
                                dubArr[coordArr[0]][coordArr[1]-1] = 'B';
                            };
                        }
                        else {alert('Invalid Move')}
            }
                
        
        }
        initMap(dubArr);
    }
}


let gGameBoard = document.querySelector('#gameMap');
function splitMap(badMap) {
    let newMap = [];
    for (i=0;i<badMap.length - 1;i++) {
        newMap.push(badMap[i].split(''));
        console.log(i);
        console.log(newMap);
    }
    return newMap;
}
//let map = splitMap(ss);

function initMap (someArr) {
    for (k = gGameBoard.childElementCount; k != 0; k--) {gGameBoard.removeChild(gGameBoard.lastElementChild)};
    for (row in someArr) {
        const getMapBody = document.getElementById('gameMap');
        const initRow = document.createElement('div');
        initRow.id = 'row';
        initRow.dataset.rowCoordinate = row;
        getMapBody.appendChild(initRow);
        for (cell in someArr[row]) {
            const newCell = document.createElement('div');
            newCell.dataset.cellCoordinate = row + "-" + cell;
            newCell.id = row + '-' + cell;
            newCell.classList.add(cellType(someArr[row][cell]));
            initRow.appendChild(newCell);
        }
    }

};

function cellType(mapStr) {
    switch (mapStr) {
    case (mapStr === " "): 
        var type = 'empty';
    
    case (mapStr === "W"): 
        var type = 'wall';
    
    case (mapStr === "S"): 
        var type = 'player';
    case (mapStr === "F"):
        var type = 'finish'
    case (mapStr==='O'):
        var type = 'emptyStore';
    case (mapStr==='X'):
        var type = 'fullStore';
    case (mapStr==='B'):
        var type = 'empty box';
    case (mapStr==='SO'):
        var type = "emptyStore player";        
    }
    return type;
};

// function navigation(directionStr, coordinateStr, dubArr) {
//     let coordArr = coordinateStr.split('-');
//     coordArr[0] = Number(coordArr[0]);
//     coordArr[1] = Number(coordArr[1]);
//     if (directionStr == 'ArrowRight') {
//         let destination = dubArr[coordArr[0]][coordArr[1]+1];
//         if (destination != 'W'&& destination != null){
//             if(destination == 'F') { alert("Winner!")}
//             else{
//                 dubArr[coordArr[0]][coordArr[1]] = ' ';
//                 dubArr[coordArr[0]][coordArr[1]+1] = 'S';
//                 console.log(dubArr);
//                 initMap(dubArr);
//             }
//         }
//         else {alert('Invalid move')};
//     }
//     else if (directionStr == 'ArrowLeft') {
//         let destination = dubArr[coordArr[0]][coordArr[1]-1];
//         if (destination != 'W'&& destination != null){
//             if(destination == 'F') { alert("Winner!")}
//             else{
//                 dubArr[coordArr[0]][coordArr[1]] = ' ';
//                 dubArr[coordArr[0]][coordArr[1]-1] = 'S';
//                 initMap(dubArr);
//             }
//         }
//         else {alert('Invalid move')};
//     }
//     else if (directionStr == 'ArrowUp') {
//         let destination = dubArr[coordArr[0]-1][coordArr[1]];
//         if (destination != 'W'&& destination != null){
//             if(destination == 'F') { alert("Winner!")}
//             else{
//                 dubArr[coordArr[0]][coordArr[1]] = ' ';
//                 dubArr[coordArr[0]-1][coordArr[1]] = 'S';
//                 initMap(dubArr);
//             }
//         }
//         else {alert('Invalid move')};
//     }
//     else if (directionStr == 'ArrowDown') {
//         let destination = dubArr[coordArr[0]+1][coordArr[1]];
//         if (destination != 'W'&& destination != null){
//             if(destination == 'F') { alert("Winner!")}
//             else{
//                 dubArr[coordArr[0]][coordArr[1]] = ' ';
//                 dubArr[coordArr[0]+1][coordArr[1]] = 'S';
//                 initMap(dubArr);
//             }
//         }
//         else {alert('Invalid move')};
//     }
//     else {alert("Invalid move")}

// }


function sokobanWin (arAr) {
    let boxCount = 0;
    for (i in arAr) {
        for (n in i) {
            if (arAr[i][n]=='B') {
                boxCount++;
            }

        }
    }
    if (boxCount == 0) {alert('You Win!')}; 
}

function movePiece(key) {
    console.log(key.code);
    //const currentPosition = document.querySelector('.player').dataset.cellCoordinate;
    Player.move(key.code, map);
    sokobanWin(map);
}
let map = splitMap(gMap);
initMap(map);
Player = movingObject('player', 'player', document.querySelector('.player').dataset.cellCoordinate);
document.addEventListener('keydown', movePiece);
